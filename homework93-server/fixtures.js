const mongoose = require('mongoose');
const config = require('./config');
const User =require('./models/User');


mongoose.connect(config.db);

const db = mongoose.connection;

const collection = ['messages', 'users'];

db.once('open', async () => {

    collection.forEach(async() => {
        try {
            await db.dropCollection('messages');
            await db.dropCollection('users');

        } catch (e) {
            console.log('Collections were not present, skipping drop...');
        }
    });

    await User.create({
        username: 'user',
        password: '123',
        role: 'user'
    }, {
        username: 'admin',
        password: 'admin123',
        role: 'admin'
    });

    db.close();
});
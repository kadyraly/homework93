const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const app = express();
const config = require("./config");
require('express-ws')(app);
app.use(cors());
app.use(express.json());

const messages = require('./app/messages');
const users = require('./app/users');

mongoose.connect(config.db);

const db = mongoose.connection;

db.once('open', () => {
    console.log('Mongoose connected!');

    app.use('/chat', messages());
    app.use('/users', users());

    const port = process.env.Port || 8000;

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});



const express = require('express');
const User = require('../models/User');
const Message = require('../models/Message');
const clients = {};

const broadcast = message => {
    Object.values(clients).forEach(client => {
        client.connection.send(JSON.stringify(message));
    });
};

const createMessage = async (text, user) => {
    const message = new Message({
        text: text,
        user: user._id
    });
    await message.save();

    message.user = user;

    broadcast({type: 'NEW_MESSAGE', message});
};

const deleteMessage = async id => {
    const message = await Message.findByIdAndDelete(id);

    broadcast({type: 'DELETE_MESSAGE', id});
};


const sendLastMessages = async (ws) => {
    const lastMessages = await Message.find().populate('user').sort({datetime: -1}).limit(30);
    ws.send(JSON.stringify({
        type: 'LAST_MESSAGES',
        messages: lastMessages
    }));
};

const sendLoggedInUsers = (ws) => {
    const users = Object.values(clients).map(client => client.user);
    ws.send(JSON.stringify({
        type: 'LOGGED_IN_USERS',
        users
    }));
};

const configureConnection = (ws, req) => {
    const id = req.get('sec-websocket-key');
    const user = req.user;

    clients[id] = {connection: ws, user: user};

    sendLoggedInUsers(ws);
    sendLastMessages(ws);

    broadcast({type: 'USER_LOGGED_IN', user});
    console.log('Client connected. Username: ', user.username);
    console.log('Number of active connections: ', Object.values(clients).length);


    ws.on('message', (msg) => {
        let decodedMessage;
        try {
            decodedMessage = JSON.parse(msg);
        } catch (e) {
            return ws.send(JSON.stringify({
                type: 'ERROR',
                message: 'Message is not JSON'
            }));
        }

        switch (decodedMessage.type) {
            case 'CREATE_MESSAGE':
                createMessage(decodedMessage.text, user);
                break;
            case 'DELETE_MESSAGE':
                if (user.role !== 'admin') {
                    return ws.send(JSON.stringify({
                        type: 'ERROR',
                        message: 'Unauthorized'
                    }));
                } else {
                    deleteMessage(decodedMessage.id);
                }
                break;
            default:
                return ws.send(JSON.stringify({
                    type: 'ERROR',
                    message: 'Unknown message type'
                }));
        }
    });

    ws.on('close', (msg) => {
        delete clients[id];
        console.log('client disconnected');
        console.log('Number of active connections', Object.values(clients).length);
        broadcast({type: 'USER_LOGGED_OUT', user});
    });
};

const createRouter = () => {
    const router = express.Router();

    router.ws('/', async (ws, req) => {

        const token = req.query.token;

        console.log(token)

        const user = await User.findOne({token});

        if(!user) {
            ws.send(JSON.stringify({type: 'ERROR', message: 'not authenticated'}));
            return ws.close();
        }

        req.user = user;
        configureConnection(ws, req);
    });

    return router;

};


module.exports = createRouter;
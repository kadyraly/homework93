import {CREATE_MESSAGE_SUCCESS, FETCH_MESSAGES_SUCCESS} from "../action/actionTypes";


const initialState = {
    messages: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_MESSAGES_SUCCESS:
            return {...state, messages: action.messages};
        case CREATE_MESSAGE_SUCCESS:
            const messages = [...state.messages];
            messages.push(action.message);
            return {...state, messages};
        default:
            return state;
    }
};

export default reducer;
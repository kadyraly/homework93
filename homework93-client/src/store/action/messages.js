

import {CREATE_MESSAGE_SUCCESS, FETCH_MESSAGES_SUCCESS} from "./actionTypes";



export const fetchMessages = messages => {
    return {type: FETCH_MESSAGES_SUCCESS, messages};
};





export const createMessage = (message)=> {
    return {type: CREATE_MESSAGE_SUCCESS, message};
};
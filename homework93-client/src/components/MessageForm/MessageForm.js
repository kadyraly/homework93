import React, {Component, Fragment} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";

class MessageForm extends Component {
    state = {
        message: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.onSubmit({message: this.state.message});
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {

        return (
            <Fragment>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormGroup controlId="message">
                        <Col componentClass={ControlLabel} sm={2}>
                            Comment
                        </Col>
                        <Col sm={10}>
                            <FormControl
                                type="text" required
                                placeholder="Enter message"
                                name="message"
                                value={this.state.message}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button bsStyle="primary" type="submit">Send</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}


export default MessageForm;
import React, {Fragment} from "react";
import {MenuItem, Nav, NavDropdown} from "react-bootstrap";

const UserName = ({user, logout}) => {
    const navTitle = (
        <Fragment>
            hello, <b>{user.username}</b>!
        </Fragment>
    );
    return (
        <Nav pullRight>
            <NavDropdown title={navTitle} id="user-menu">
                <MenuItem divider/>
                <MenuItem onClick={logout}>Logout</MenuItem>
            </NavDropdown>
        </Nav>
    )

};

export  default  UserName;
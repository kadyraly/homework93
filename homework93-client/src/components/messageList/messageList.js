import React from 'react';
import {Panel} from "react-bootstrap";
import PropTypes from 'prop-types';

const MessageList = props => {

    return (
        <Panel>
            <Panel.Body>
                {props.message}
            </Panel.Body>

        </Panel>
    );
};

MessageList.propTypes = {
    id: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired
};

export default MessageList;
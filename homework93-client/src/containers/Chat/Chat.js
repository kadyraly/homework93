import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {createMessage, fetchMessages} from "../../store/action/messages";

class Chat extends Component {

    state = {
        username: '',
        usernameSet: false,
        messages: [],
        messageText: '',
        users: [],
        redirect: false
    };

    messageTextChangeHandler = event => {
        this.setState({messageText: event.target.value})
    };

    componentDidMount () {
        if(!this.props.user) {
            this.props.history.push('/login');
        } else {
            this.websocket = new WebSocket('ws://localhost:8000/chat?token=' + this.props.user.token);
            this.websocket.onmessage = (message) => {
                const decodedMessage = JSON.parse(message.data);

                switch (decodedMessage.type) {
                    case 'NEW_MESSAGE':
                        this.props.createMessage(decodedMessage.message);
                        this.setState(prevState => {
                            return {messages: [...prevState.messages, decodedMessage.message]}
                        });
                        break;
                    case 'LOGGED_IN_USERS':
                        this.setState({users: decodedMessage.users});
                        break;
                    case 'LAST_MESSAGES':
                        this.props.fetchMessages(decodedMessage.messages);
                        this.setState({messages: decodedMessage.messages});
                        break;
                    case 'USER_LOGGED_IN':
                        this.setState(prevState => {
                            const newUser = decodedMessage.user;
                            const userIndex = prevState.users.findIndex(user => user._id === newUser._id);
                            if (userIndex === -1) {
                                return {users: [...prevState.users, decodedMessage.user]}
                            }
                        });
                        break;
                    case 'DELETE_MESSAGE':
                        this.setState(prevState => {
                            const messages = prevState.messages.filter(message => message._id !== decodedMessage.id);
                            return {messages};
                        });
                        break;
                    default:

                        console.log(decodedMessage);
                }
            }
        }

    }
    sendUsername = event => {
        event.preventDefault();
        this.websocket.send(JSON.stringify({
            type: 'SET_USERNAME',
            username: this.state.username
        }));

        this.setState({usernameSet: true})
    };
    sendMessage = event => {
        event.preventDefault();
        this.websocket.send(JSON.stringify({
            type: 'CREATE_MESSAGE',
            text: this.state.messageText
        }));

        this.setState({messageText: ''})
    };

    removeMessage = (event, id) => {
        event.preventDefault();

        this.websocket.send(JSON.stringify({
            type: 'DELETE_MESSAGE',
            id
        }));
    };

    render() {
        return (
                <Fragment>
                    <div className="Users floatBlock">
                        <h3>Users</h3>
                        {this.state.users.map(user => (
                            <p key={user.username}>
                                <b>{user.username}</b>
                            </p>
                        ))}
                    </div>
                    <div className="Messages floatBlock">
                        <h3>Messages</h3>
                        {this.state.messages.map(message => (
                            <p key={message._id}><b>{message.user.username}: </b>{message.text}
                                {this.props.user.role === 'admin' ? <a href="#" onClick={event => this.removeMessage(event, message._id)}>Remove</a> : null}
                            </p>
                        ))}
                        <form onSubmit={this.sendMessage}>
                            <input type="text" required placeholder="Enter message"
                                   value={this.state.messageText} onChange={this.messageTextChangeHandler}/>
                            <button type="submit">Send</button>
                        </form>
                    </div>

                </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        messages: state.messages.messages
    }
};

const mapDispatchToProps = dispatch => {
    return {
        createMessage: (message) => dispatch(createMessage(message)),
        fetchMessages: (messages) => dispatch(fetchMessages(messages))
    }

};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
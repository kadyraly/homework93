import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

class Message extends Component {
    componentDidMount() {
        this.props.onFetchMessages(this.props.postId);
    }



    render() {

        return (
            <Fragment>
                <PageHeader>
                    Messages
                </PageHeader>

                {this.props.messages.map(message => (
                    <messageList
                        key={message._id}
                        id={message._id}
                        message={message.message}
                        user={message.user.username}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        messages: state.messages.messages,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchMessages: (id) => dispatch(fetchMessages(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Message);